<?php
/**
 * Created by PhpStorm.
 * User: alexandre
 * Date: 25/09/2017
 * Time: 08:29
 */

namespace LogBundle\Services;

use Symfony\Bridge\Monolog\Logger;

/**
 * Class LoggerService
 * @package CoreBundle\Services
 */
class LoggerService
{

    /**
     * @var Logger $logger
     */
    protected $logger;

    /**
     * @var string $thread
     */
    protected $thread;

    /**
     * @var array $globalKeysValues
     */
    protected $globalKeysValues;

    /**
     * LoggerService constructor.
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
        $this->thread = 'Symfony';
        $this->globalKeysValues = [];
    }

    /**
     * Set log thread
     * @param string $thread
     */
    public function setThread(string $thread)
    {
        $this->thread = $thread;
    }

    /**
     * @param array $globalKeysValues
     */
    public function setGlobalKeysValues(array $globalKeysValues)
    {
        if (\count($this->globalKeysValues) > 0) {
            $this->globalKeysValues = array_merge($this->globalKeysValues, $globalKeysValues);
        } else {
            $this->globalKeysValues = $globalKeysValues;
        }
    }

    /**
     * Log les messages
     * @param string $lvl
     * @param string $msg
     * @param array $context
     */
    public function logMsg(string $lvl, string $msg, array $context = [])
    {

        $context = array_merge($context, $this->globalKeysValues);
        $lvl = strtolower($lvl);

        if (\in_array($lvl, ['debug', 'info', 'notice', 'warning', 'error', 'critical', 'alert', 'emergency'])) {
            $this->logger->{$lvl}($msg, ['thread' => $this->thread, 'keysvalues' => $context]);
        } else {
            // On log si la criticité n'est pas standard
            $this->logger->warning('Criticité du log incorrecte', [
                'thread' => $this->thread,
                'keysvalues' => array_merge(['msg' => $msg, 'lvl' => $lvl], $context)
            ]);
        }

    }

}