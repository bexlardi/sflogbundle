<?php
/**
 * Created by PhpStorm.
 * User: alexandre
 * Date: 01/08/2017
 * Time: 13:51
 */

namespace LogBundle\Services;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class MonologCustomProcessor
 * @package CoreBundle\Services
 */
class MonologCustomProcessor
{

    protected $tokenStorage;

    /**
     * MonologCustomProcessor constructor.
     * @param TokenStorage $tokenStorage
     */
    public function __construct($tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Ajoute des données pour les logs
     * @param array $record
     * @return array
     */
    public function processRecord(array $record): array
    {

        if (isset($record['context']['thread'])) {
            $record['extra']['thread'] = $record['context']['thread'];
            unset($record['context']['thread']);
        } else {
            $record['extra']['thread'] = 'Symfony';
        }

        // Mise en place des couples clé/valeur
        if (isset($record['context']['keysvalues'])) {
            $keysvalues = $record['context']['keysvalues'];            
        } else {
            $keysvalues = [];
        }
        unset($record['context']['keysvalues']);

        // Récupération automatique de l'user si disponible
        $token = $this->tokenStorage->getToken();
        if (null !== $token && \is_object($token->getUser())) {
            $keysvalues = array_merge($keysvalues, ['user' => $token->getUser()->getId()]);
        }

        // Mise en forme des couples clé/valeur
        $record['extra']['keysvalues'] = str_replace([':', '"'], ['=', ''], json_encode($keysvalues));

        return $record;
    }

}