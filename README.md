Bexlardi - Log Bundle for Symfony
========================================

Installation
------------
Dans le fichier composer.json : 
- ajouter la dépendance dans les composants requis => "bexlardi/symfonylogbundle": "^1.0"


Mettre à jour les dépendances composer => composer update

Dans le fichier AppKernel, ajouter le bundle => new LogBundle\BexlardiLogBundle()

Dans les fichiers de configuration (config_dev.yml et config_prod.yml par défault), dans monolog/handlers/main ajouter la ligne suivante => formatter: monolog.formatter.session_request

Usage
-----

Appeler le service pharmagest.logger.service (injection de dépendances ou $this->container->get('pharmagest.logger.service')).

Ajout d'un thread manuel dans le log ('Symfony' par défaut) :
$this->logger->setThread($string);

Exemple :
$this->logger->setThread('CoreBundle');

Ajout de couples clés/valeurs de manière globale (partagé entre plusieurs logs) :
$this->logger->setGlobalKeysValues($array);

Exemple : 
$this->logger->setGlobalKeysValues([
	'service' => 'UpdateService',
	'function'     => 'updateMaterializedViews',
	'target' => $target
]);

Création d'un message de log : 
$this->logger->logMsg(String $level, String $message, Array $extra);

$this->logger->logMsg('info', 'Message de log', [
	'target' => $target
]);

Si l'utilisateur est authentifié, un champs user avec l'id de l'utilisateur est automatiquement ajouté.

Requirements
------------

Symfony 3 ou 4, PHP 7